Prerequisites
-------------
1) Apache 2.2 (x86, tested with 2.2.31)
2) Python 2.5 (x86, tested with 2.5.4)
3) mod_python 3.3 (x86, tested with 3.3.1)
4) np python package from https://bitbucket.org/niteshpatel/python-packages

Installation
------------
1) Clone the repo to a local folder
2) Add apacheconf/nitesh.no-ip.net.conf to Apache includes
3) Update apacheconf/nitesh.no-ip.net.conf with environment specific info
4) Add the full path to testing/pyfiles to a pth file on the Python path
