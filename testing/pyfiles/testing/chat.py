# Basic chat server (demo for XMLHTTP)

class ChatServer:
    MAX_LINES = 20
    
    def __init__(self):
        self._history = []
        
    def write(self, user, text):
        if user and text:
            self._history = [(user, text)] + self._history[:ChatServer.MAX_LINES-1]

    def read(self):
        return 'new Array(%s)' % ', '.join(
            [('new Array("%s", "%s")' % (user, text.replace('"', '\\"'))) for user, text in self._history])

chatServer = ChatServer()

def processor(form):
    chatServer.write(form.get('user'), form.get('text'))
    return chatServer.read()
