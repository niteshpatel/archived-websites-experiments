from np.text import wordlist

FILE_PATH = r'C:\Documents and Settings\Nitesh Patel\My Documents\My Office\extra_dicts\scrabbledict.txt'
wordList = wordlist.WordMatcher(FILE_PATH)

def processor(form):
    matchList, limited = wordList.returnMatches(wordpart=form.get('query', ''), limit=10)
    data = writeOptionsToSelect(matchList, limited=limited)
    return data

def writeOptionsToSelect(listOptions, limited=False):
    strJS = ''
    strJS += """
        targetObj = document.getElementById("XMLHTTPresult")
        targetObj.options.length = 0
    """
    for option in listOptions:
        strJS += 'targetObj.options[targetObj.options.length] = new Option("%s", "")\n' % option.strip()
    if limited:
        strJS += "targetObj.options[targetObj.options.length] = new Option('...', '')"
    strJS += """
        targetObj.size = targetObj.options.length + 1
        if (targetObj.size > 12) {
            targetObj.size = 12
        }
        if (targetObj.options.length == 1) {
            targetObj.options[0].selected = true;
        }
        if (targetObj.options.length == 0) {
            targetObj.size = 2
        }
    """
    return strJS                               
