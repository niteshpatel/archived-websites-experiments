// JavaScript Document

/* Globals for monitoring if an input is changed since the last check */
var interactMonitorId = false;
var interactPreviousValue;
var interactProcessor;

/* This checks the input every once a while to see if it has changed */
function monitorInput(elementId) {
	if (interactPreviousValue == document.getElementById(elementId).value) {
		window.clearInterval(interactMonitorId);
		interactMonitorId = false;
		interactProcessor(interactPreviousValue);
	}
	else {
		interactPreviousValue = document.getElementById(elementId).value;
	}
}

/* PUBLIC: Start monitoring if we are not already */
function beginMonitor(elementId, processor, timeout) {
	if (timeout === undefined) {
		timeout = 500;
	}
	if (interactMonitorId === false) {
		interactProcessor = processor;
		interactPreviousValue = document.getElementById(elementId).value;
		interactMonitorId = window.setInterval("monitorInput('"+elementId+"')", timeout);
	}
}
