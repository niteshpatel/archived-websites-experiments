var xmlHttp = null;

/* PUBLIC: Sends the request to the server and sets up an event for when the 
	server responds with a non-empty response */
function sendRequest(queryTarget, processor) {
	if (xmlHttp && xmlHttp.readyState !== 4) {
		xmlHttp.abort();
	}
	xmlHttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
	if (xmlHttp) {
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4) {
				processor(xmlHttp.responseText);
			}
		};
		xmlHttp.open("GET", queryTarget, true);
		xmlHttp.send(null);
	}
}
