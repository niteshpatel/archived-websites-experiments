// JavaScript Document


/* Set-up auto-refresh */
window.setInterval("refreshPage()", 500);

/* Loads the chat history from the server */
function refreshPage() {
	// We add the time to the request otherwise IE caches the response
	sendRequest("asplite.py?module=chat&function=processor&time=" + new Date().getTime(), processResponse);
}

/* Sends the request - if this function was called by the auto-refresh we
  override the usual requirements of username and text */
function sendQuery(e) {
	var keyCode = document.all ? event.keyCode : e.keyCode;
	if (keyCode == 13) {
		var user = document.getElementById('userName').value;
		var text = document.getElementById('userText').value;
		if (user !== "" && text !== "") {
            // We add the time to the request otherwise IE caches the response
			sendRequest("asplite.py?module=chat&function=processor&user=" + user + "&text=" + text + "&time=" + new Date().getTime(), processResponse);
			document.getElementById('userText').value = "";
		}	
	}
}

/* Handles the response text */
function processResponse(responseText) {
    if (!responseText) return; 
	var chatHistory = eval(responseText);
	var historyString = '';
	for (i=chatHistory.length-1; i>-1; i--) {  // We want to display entries bottom-up
		historyString += chatHistory[i][0] + ': ' + chatHistory[i][1] + '\n';
	}
	document.getElementById('historyText').value = historyString;
}
